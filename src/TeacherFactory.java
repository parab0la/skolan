import java.util.Scanner;

public class TeacherFactory extends PersonFactory implements Factory<Teacher> {

	public TeacherFactory(Scanner scan) {
		super(scan);
	}
	
	@Override
	public Teacher create() {
		
		System.out.println("Vad kallas l�raren?: ");
		String name = currentScanner().nextLine();
		
		Teacher newTeacher = new Teacher(name, Subject.JAVA, Subject.PYTHON);
		
		return newTeacher;
	}
}
