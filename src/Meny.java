import java.util.*;

public class Meny {

	private Scanner reader;
	
	private Skola skolan;
	private StudentFactory studentFact;
	private TeacherFactory teacherFact;
	
	
	public Meny() {
		skolan = new Skola();
		studentFact = new StudentFactory(reader);
		teacherFact = new TeacherFactory(reader);
		
	}
	
	public void runMenu(){
		boolean run = true;
		
		do{
			int input = printChoices();
			
			switch(input){
			
			case 1:
				createPerson(studentFact);
				
				break;
			case 2:
				createPerson(teacherFact);
				
				break;
			case 3:
				skolan.printStudents();
				break;
			case 4:
				skolan.printTeachers();
				break;
				default:
					run = false;
					break;
			
			}
			
		}while(run);
		
		skolan.save();
	}
	
	private <T extends Person>void createPerson(Factory<T> factory){
		
		Person person = factory.create();
		skolan.addPerson(person);
	}
	
	private int printChoices(){
		
		System.out.println("Hej och všlkommen");
		System.out.println("1: Add student");
		System.out.println("2. Add teacher");
		System.out.println("3: Print students");
		System.out.println("4: Print teachers");
		reader = new Scanner(System.in);
		int input = reader.nextInt();
		
		return input;
	}
}
