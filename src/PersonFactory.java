import java.util.Scanner;

public abstract class PersonFactory {

	private Scanner scanner;
	
	public PersonFactory(Scanner scan) {
		this.scanner = scan;
	}
	
	protected Scanner currentScanner(){
		scanner = new Scanner(System.in);
		return scanner;
		
	}
	
}
